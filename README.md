# @html-validate/prettier-config

> HTML-Validate prettier config.

## Install

```
npm install --save-dev @html-validate/prettier-config
```

## Usage

```
npm pkg set prettier="@html-validate/prettier-config"
npm pkg set scripts.prettier:check="prettier --check ."
npm pkg set scripts.prettier:write="prettier --write ."
```

Or manually edit `package.json`:

```json
{
  "scripts": {
    "prettier:check": "prettier --check .",
    "prettier:write": "prettier --write ."
  },
  "prettier": "@html-validate/prettier-config"
}
```

Prettier does not need to be installed separately, this package includes `prettier` and its binary.

## lint-staged

To use lint-staged (included) configure the `pre-commit` hook to run husky with the `--config` flag:

```bash
#!/bin/sh

configfile=$(node -p 'require.resolve("@html-validate/prettier-config/lint-staged")')
exec npm exec lint-staged -- --config "${configfile}"
```

Lint-staged does not need to be installed separately, this package includes `lint-staged` and its binary.
