module.exports = {
	plugins: [require.resolve("prettier-plugin-packagejson")],

	tabWidth: 2,
	useTabs: true,
	printWidth: 100,

	overrides: [
		{
			files: "**/*.md",
			options: {
				useTabs: false,
			},
		},
		{
			files: "**/{*,.*}.json",
			options: {
				useTabs: false,
			},
		},
	],
};
