#!/usr/bin/env node

const fs = require("node:fs");
const path = require("node:path");
const { spawn } = require("child_process"); // eslint-disable-line security/detect-child-process

const pkgPath = path.dirname(require.resolve("prettier/package.json"));
const binary = [
	path.join(pkgPath, "bin/prettier.cjs"), // prettier 3.x
	path.join(pkgPath, "bin-prettier.js"), // prettier 2.x
].find((it) => fs.existsSync(it));

if (!binary) {
	throw new Error("Failed to locate prettier executable");
}

spawn("node", [binary, ...process.argv.slice(2)], {
	stdio: "inherit",
}).on("exit", (code) => {
	/* eslint-disable-next-line no-process-exit */
	process.exit(code);
});
