#!/usr/bin/env node

const path = require("path");
const { spawn } = require("child_process"); // eslint-disable-line security/detect-child-process

const pkgPath = path.dirname(require.resolve("lint-staged/package.json"));
const binary = path.join(pkgPath, "bin/lint-staged.js");

spawn("node", [binary, ...process.argv.slice(2)], {
	stdio: "inherit",
}).on("exit", (code) => {
	/* eslint-disable-next-line no-process-exit */
	process.exit(code);
});
