# @html-validate/prettier-config changelog

## 2.5.14 (2025-03-02)

### Bug Fixes

- **deps:** update dependency prettier-plugin-packagejson to v2.5.9 ([f0c8b20](https://gitlab.com/html-validate/prettier-config/commit/f0c8b2036b699220e611d0089245b500f236292d))

## 2.5.13 (2025-02-02)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.4.3 ([d1b8831](https://gitlab.com/html-validate/prettier-config/commit/d1b88310edefa05e445d0a08c9aa30011664330e))

## 2.5.12 (2025-01-26)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.4.2 ([a8b1820](https://gitlab.com/html-validate/prettier-config/commit/a8b18207d14bde232feb261935eb25ed5614b8ad))

## 2.5.11 (2025-01-19)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.4.0 ([880c1c0](https://gitlab.com/html-validate/prettier-config/commit/880c1c0ed0c57860335a4d8417bf1e0456202159))
- **deps:** update dependency lint-staged to v15.4.1 ([983d7e9](https://gitlab.com/html-validate/prettier-config/commit/983d7e9e150eb8948fb798430af411ffccadb6d5))
- **deps:** update dependency prettier-plugin-packagejson to v2.5.7 ([46db391](https://gitlab.com/html-validate/prettier-config/commit/46db3919984476ed230ec4fe417ec8c1466d1fe1))
- **deps:** update dependency prettier-plugin-packagejson to v2.5.8 ([02ba2a3](https://gitlab.com/html-validate/prettier-config/commit/02ba2a3965159bb7a437eab4f8cdcdcc8681ea3e))

## 2.5.10 (2025-01-05)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.3.0 ([3379f92](https://gitlab.com/html-validate/prettier-config/commit/3379f92402cdb9a4280e38a62620ea81823862b4))

## 2.5.9 (2024-12-15)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.11 ([bba227b](https://gitlab.com/html-validate/prettier-config/commit/bba227b10108ab43b4ae26da8873148f1ac29ab6))

## 2.5.8 (2024-12-01)

### Bug Fixes

- **deps:** update dependency prettier-plugin-packagejson to v2.5.6 ([22d9e72](https://gitlab.com/html-validate/prettier-config/commit/22d9e72bb61211a3b1d34671b54f26f140b6b930))

## 2.5.7 (2024-11-24)

### Bug Fixes

- **deps:** update dependency prettier-plugin-packagejson to v2.5.4 ([fbc508d](https://gitlab.com/html-validate/prettier-config/commit/fbc508dbd93d1f2c85594bf26948c90fb8b1eecc))
- **deps:** update dependency prettier-plugin-packagejson to v2.5.5 ([1ef16f6](https://gitlab.com/html-validate/prettier-config/commit/1ef16f61d7e54618bfc8d19c1839594d190fa483))

## 2.5.6 (2024-10-13)

### Bug Fixes

- **deps:** update dependency prettier-plugin-packagejson to v2.5.3 ([ff37d5d](https://gitlab.com/html-validate/prettier-config/commit/ff37d5d0e7194588ba00834b38b9b2ad76b2256e))

## 2.5.5 (2024-09-08)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.10 ([680a26c](https://gitlab.com/html-validate/prettier-config/commit/680a26c6e56a54b9dcf736d9234d1459b413a0f5))

## 2.5.4 (2024-09-01)

### Bug Fixes

- **deps:** update dependency prettier-plugin-packagejson to v2.5.2 ([b30c59f](https://gitlab.com/html-validate/prettier-config/commit/b30c59f2d695f19782649ec0eac059df2c66ceb3))

## 2.5.3 (2024-08-25)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.9 ([c52e8bc](https://gitlab.com/html-validate/prettier-config/commit/c52e8bcc342ee3084821fa2d6169837327f5398b))

## 2.5.2 (2024-08-11)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.8 ([2482c1b](https://gitlab.com/html-validate/prettier-config/commit/2482c1b5dec3a41d1874b635af88d1cda60437f5))

## 2.5.1 (2024-07-21)

### Bug Fixes

- **deps:** update dependency prettier-plugin-packagejson to v2.5.1 ([77e358e](https://gitlab.com/html-validate/prettier-config/commit/77e358e4ca00f8d6ffcfdec7477d8e1fba9dea72))

## 2.5.0 (2024-07-14)

### Features

- use prettier-plugin-packagejson to sort keys of package.json ([600598d](https://gitlab.com/html-validate/prettier-config/commit/600598d5ef4bb749ee2b9b4bb11aea6040377198))

## 2.4.15 (2024-06-16)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.6 ([428cb88](https://gitlab.com/html-validate/prettier-config/commit/428cb887461b941099cc3451cd552251592ff1db))
- **deps:** update dependency lint-staged to v15.2.7 ([b39dec0](https://gitlab.com/html-validate/prettier-config/commit/b39dec04bc0a640eba37021ec8550f18e76de628))

## 2.4.14 (2024-06-02)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.5 ([8e3d9fc](https://gitlab.com/html-validate/prettier-config/commit/8e3d9fc6bca8948ca1e29e833ce039bf2be879e5))

## 2.4.13 (2024-05-26)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.4 ([73fffcb](https://gitlab.com/html-validate/prettier-config/commit/73fffcb655ccf3f449ea05e33b5deaa9c13ad1bd))

## [2.4.12](https://gitlab.com/html-validate/prettier-config/compare/v2.4.11...v2.4.12) (2024-2-11)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.2 ([38b226d](https://gitlab.com/html-validate/prettier-config/commit/38b226d2589a5d165c31aed38c7ab7374facfe39))

## [2.4.11](https://gitlab.com/html-validate/prettier-config/compare/v2.4.10...v2.4.11) (2024-2-4)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.1 ([b81880a](https://gitlab.com/html-validate/prettier-config/commit/b81880a9e81a9f5fca11a2073c9923c1ac045538))

## [2.4.10](https://gitlab.com/html-validate/prettier-config/compare/v2.4.9...v2.4.10) (2023-12-10)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.2.0 ([b046de7](https://gitlab.com/html-validate/prettier-config/commit/b046de724556b73d698414cf62a8e40f1ed0cd92))

## [2.4.9](https://gitlab.com/html-validate/prettier-config/compare/v2.4.8...v2.4.9) (2023-11-19)

### Bug Fixes

- **deps:** update dependency lint-staged to v15.1.0 ([06eebdc](https://gitlab.com/html-validate/prettier-config/commit/06eebdc2c4c6aed71c4e92e11abaddcc7d2d8b5b))

## [2.4.8](https://gitlab.com/html-validate/prettier-config/compare/v2.4.7...v2.4.8) (2023-10-22)

### Bug Fixes

- **deps:** update dependency lint-staged to v15 ([a2636d6](https://gitlab.com/html-validate/prettier-config/commit/a2636d66e0844721f32833e15cbfe6796c684dbf))

## [2.4.7](https://gitlab.com/html-validate/prettier-config/compare/v2.4.6...v2.4.7) (2023-08-27)

### Dependency upgrades

- **deps:** update dependency lint-staged to v14.0.1 ([e59a010](https://gitlab.com/html-validate/prettier-config/commit/e59a01035bb76f9132dd6eabedabd3b213b7c23a))

## [2.4.6](https://gitlab.com/html-validate/prettier-config/compare/v2.4.5...v2.4.6) (2023-08-20)

### Dependency upgrades

- **deps:** update dependency lint-staged to v14 ([215787f](https://gitlab.com/html-validate/prettier-config/commit/215787f8f95bc20cbe7d310ac46c40c220eb713d))

## [2.4.5](https://gitlab.com/html-validate/prettier-config/compare/v2.4.4...v2.4.5) (2023-08-15)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.3.0 ([66f8eaa](https://gitlab.com/html-validate/prettier-config/commit/66f8eaa8d8cd13a5dd52d53dca9c629f1d1a1103))

## [2.4.4](https://gitlab.com/html-validate/prettier-config/compare/v2.4.3...v2.4.4) (2023-08-13)

### Bug Fixes

- require nodejs 16 or later ([70a5c86](https://gitlab.com/html-validate/prettier-config/commit/70a5c86af5af08d3c8a1a4591c0bef973b3991f9))

## [2.4.3](https://gitlab.com/html-validate/prettier-config/compare/v2.4.2...v2.4.3) (2023-08-06)

### Dependency upgrades

- **deps:** update dependency prettier to 3.0.0 || 2.8.8 || 3.0 ([e7498a1](https://gitlab.com/html-validate/prettier-config/commit/e7498a1bd1aae6bd2fb68f75998d109611d10d9e))

## [2.4.2](https://gitlab.com/html-validate/prettier-config/compare/v2.4.1...v2.4.2) (2023-07-28)

### Bug Fixes

- prettier 2.x path ([e08db46](https://gitlab.com/html-validate/prettier-config/commit/e08db4603c8d949edf410c8702623d7a71b294ca))

## [2.4.1](https://gitlab.com/html-validate/prettier-config/compare/v2.4.0...v2.4.1) (2023-07-28)

### Bug Fixes

- temporary support both prettier 2.x and 3.x ([9fc14e7](https://gitlab.com/html-validate/prettier-config/commit/9fc14e713e07e95c2d3ff5865651d16a9d6895d3))

### Dependency upgrades

- **deps:** pin dependency prettier to 3.0.0 ([972b8e7](https://gitlab.com/html-validate/prettier-config/commit/972b8e73ead6381fb75aef5b815c3e68265152aa))

## [2.4.0](https://gitlab.com/html-validate/prettier-config/compare/v2.3.11...v2.4.0) (2023-07-15)

### Features

- **deps:** update dependency prettier to v3 ([7244d00](https://gitlab.com/html-validate/prettier-config/commit/7244d006d69689e3f98ea750e79d5ce1415d4ada))

## [2.3.11](https://gitlab.com/html-validate/prettier-config/compare/v2.3.10...v2.3.11) (2023-07-02)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.2.3 ([721ce2a](https://gitlab.com/html-validate/prettier-config/commit/721ce2a54e8da8b713ee3eb027d3b859305df552))

## [2.3.10](https://gitlab.com/html-validate/prettier-config/compare/v2.3.9...v2.3.10) (2023-04-30)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.2.2 ([3dc3643](https://gitlab.com/html-validate/prettier-config/commit/3dc364345f9b0c5f122b476e8cc2bbd993f43949))
- **deps:** update dependency prettier to v2.8.8 ([6eeb4b6](https://gitlab.com/html-validate/prettier-config/commit/6eeb4b6f56156a37d2774c7e58f015340be3bb70))

## [2.3.9](https://gitlab.com/html-validate/prettier-config/compare/v2.3.8...v2.3.9) (2023-04-09)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.2.1 ([e20cf75](https://gitlab.com/html-validate/prettier-config/commit/e20cf7589d8179896785a2c0ae91ff993216e04e))

## [2.3.8](https://gitlab.com/html-validate/prettier-config/compare/v2.3.7...v2.3.8) (2023-03-26)

### Dependency upgrades

- **deps:** update dependency prettier to v2.8.5 ([8288765](https://gitlab.com/html-validate/prettier-config/commit/82887658b43afaeb4b5ab2df45e4507cc166e539))
- **deps:** update dependency prettier to v2.8.6 ([3378f8b](https://gitlab.com/html-validate/prettier-config/commit/3378f8b0e11a220327935a66c97fad0d881e796a))
- **deps:** update dependency prettier to v2.8.7 ([5de02ea](https://gitlab.com/html-validate/prettier-config/commit/5de02ea55dd58ac2e39ff07ab85e2dbd5c61addd))

## [2.3.7](https://gitlab.com/html-validate/prettier-config/compare/v2.3.6...v2.3.7) (2023-03-19)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.2.0 ([3c72743](https://gitlab.com/html-validate/prettier-config/commit/3c727430025e06aefd644e11a34bcdb3fc59abbe))

## [2.3.6](https://gitlab.com/html-validate/prettier-config/compare/v2.3.5...v2.3.6) (2023-02-19)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.1.2 ([8f333f9](https://gitlab.com/html-validate/prettier-config/commit/8f333f9a5f9d96ed0c733ba1e9e0e4f262301d0d))

## [2.3.5](https://gitlab.com/html-validate/prettier-config/compare/v2.3.4...v2.3.5) (2023-02-12)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.1.1 ([d33b921](https://gitlab.com/html-validate/prettier-config/commit/d33b9219125e264853afa49d4c62ddac2a67ba63))
- **deps:** update dependency prettier to v2.8.4 ([242c117](https://gitlab.com/html-validate/prettier-config/commit/242c117c23397a4470c3b79ef361eba3a3b97afd))

## [2.3.4](https://gitlab.com/html-validate/prettier-config/compare/v2.3.3...v2.3.4) (2023-01-22)

### Dependency upgrades

- **deps:** update dependency prettier to v2.8.3 ([d9c181b](https://gitlab.com/html-validate/prettier-config/commit/d9c181bb9a63bbb61302e4b941fc30f39c828a7e))

## [2.3.3](https://gitlab.com/html-validate/prettier-config/compare/v2.3.2...v2.3.3) (2023-01-15)

### Dependency upgrades

- **deps:** update dependency prettier to v2.8.2 ([c02bcb1](https://gitlab.com/html-validate/prettier-config/commit/c02bcb13dfe350c42c16f0605824d9a258efa308))

## [2.3.2](https://gitlab.com/html-validate/prettier-config/compare/v2.3.1...v2.3.2) (2022-12-11)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.1.0 ([817d685](https://gitlab.com/html-validate/prettier-config/commit/817d6853f3b64e7295612eaac70059923ba3713c))
- **deps:** update dependency prettier to v2.8.1 ([da4e078](https://gitlab.com/html-validate/prettier-config/commit/da4e078155bf3f6c1aa0485f57346034442ad85f))

## [2.3.1](https://gitlab.com/html-validate/prettier-config/compare/v2.3.0...v2.3.1) (2022-11-27)

### Dependency upgrades

- **deps:** update dependency lint-staged to v13.0.4 ([f1f0d8a](https://gitlab.com/html-validate/prettier-config/commit/f1f0d8a2b693bb9d1e0f1e7ff7e475f494454613))
- **deps:** update dependency prettier to v2.8.0 ([13fb1d7](https://gitlab.com/html-validate/prettier-config/commit/13fb1d773e12a30fd5f6134e879b9d414fc47dc0))

## [2.3.0](https://gitlab.com/html-validate/prettier-config/compare/v2.2.1...v2.3.0) (2022-07-03)

### Features

- wrap lint-staged ([f6250c6](https://gitlab.com/html-validate/prettier-config/commit/f6250c6a1ef03d596fa18f51039c3c3796d5153c))

## [2.2.1](https://gitlab.com/html-validate/prettier-config/compare/v2.2.0...v2.2.1) (2022-06-19)

### Dependency upgrades

- **deps:** update dependency prettier to v2.7.0 ([915c1af](https://gitlab.com/html-validate/prettier-config/commit/915c1af4a473f2d62badd8764085e31f9154b825))
- **deps:** update dependency prettier to v2.7.1 ([4255122](https://gitlab.com/html-validate/prettier-config/commit/425512242d5349be0a2b93c70d106cf5af243e90))

## [2.2.0](https://gitlab.com/html-validate/prettier-config/compare/v2.1.0...v2.2.0) (2022-05-08)

### Features

- require node 14 ([e7cfd17](https://gitlab.com/html-validate/prettier-config/commit/e7cfd17b29e28d690bac1410052047d2bbc355e7))

## [2.1.0](https://gitlab.com/html-validate/prettier-config/compare/v2.0.0...v2.1.0) (2022-04-06)

### Features

- include wrapped prettier binary ([0f81cd1](https://gitlab.com/html-validate/prettier-config/commit/0f81cd1e349285028e09d04ad2454abd436afb71))

## [2.0.0](https://gitlab.com/html-validate/prettier-config/compare/v1.1.0...v2.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([113e541](https://gitlab.com/html-validate/prettier-config/commit/113e5419c8f8f7369492e9e190f17f388271cd7f))

# [1.1.0](https://gitlab.com/html-validate/prettier-config/compare/v1.0.1...v1.1.0) (2020-10-03)

### Features

- increase `printWidth` to 100 ([285b4cf](https://gitlab.com/html-validate/prettier-config/commit/285b4cfbe2d6ec87ae54d4c7383fa0940781a09b))

## [1.0.1](https://gitlab.com/html-validate/prettier-config/compare/v1.0.0...v1.0.1) (2020-03-29)

### Bug Fixes

- trailingComma defaults to es5 since prettier 2 ([9d33abe](https://gitlab.com/html-validate/prettier-config/commit/9d33abed50cdb4969fe4b3a6f5a98e941efd1607))

# 1.0.0 (2019-12-25)

### Bug Fixes

- formatting ([96619ae](https://gitlab.com/html-validate/prettier-config/commit/96619ae534481bafd4bfcc7dedcefc26374999d6))

### Features

- initial release refactored from html-validate ([7405428](https://gitlab.com/html-validate/prettier-config/commit/74054285fe0ee0a3526075117213bd23d560b8ec))
